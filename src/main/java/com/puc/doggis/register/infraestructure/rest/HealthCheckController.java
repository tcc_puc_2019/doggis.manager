package com.puc.doggis.register.infraestructure.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("healthCheck")
public class HealthCheckController
{

    @GetMapping(name="/", produces = "application/json")
    public String healthCheck() {
        return "Microserviço de Cadastros funcionando!";
    }
}
