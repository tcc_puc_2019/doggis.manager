package com.puc.doggis.register.infraestructure.repository.client;

import com.puc.doggis.register.domain.client.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Long>
{

    @Query("SELECT client FROM Client client where LOWER(client.name) LIKE :query")
    Page<Client> findByQueryString(@Param("query") String query, Pageable pageable);

}
