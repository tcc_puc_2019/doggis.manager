package com.puc.doggis.register.application.pet;

import com.puc.doggis.register.domain.client.Client;
import com.puc.doggis.register.domain.pet.Pet;
import org.springframework.data.domain.Page;

public interface PetService
{
    Pet findById(Long id);

    Pet create(Pet role);

    Pet update(Long id, Pet role);

    Page<Pet> findAll(Integer page, Integer pageSize, String query);

}
