package com.puc.doggis.register.configuration.label.pet;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PetLabel
{
    ID("id"),
    NAME("name"),
    TYPE("type"),
    RACE("race"),
    PET_SIZE_TYPE("petSizeType"),
    OBSERVATIONS("observations"),
    CAN_TAKE_PICTURES("canTakePictures"),
    OWNER("owner");

    private final String value;

}
