package com.puc.doggis.register.configuration.enumerated;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserType
{
    CLIENT("Cliente"),
    ADMIN("Administrador"),
    CLERK("Atendente"),
    VETERINARIAN("Veterinário");

    private final String value;
}
