package com.puc.doggis.register.configuration.label.client;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ClientLabel
{
    ID("id"),
    NAME("name"),
    EMAIL("email"),
    RG("rg"),
    CPF("cpf"),
    ADDRESS("address"),
    PATAZ_AMOUNT("patazAmount");

    private final String value;

}
