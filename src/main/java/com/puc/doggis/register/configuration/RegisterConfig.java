package com.puc.doggis.register.configuration;

import com.puc.doggis.register.configuration.commons.AbstractSerializer;
import com.puc.doggis.register.domain.client.Client;
import com.puc.doggis.register.domain.pet.Pet;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class RegisterConfig {
    @Bean(name = "objectSerializers")
    public Map<Class, AbstractSerializer> objectSerializers(@Qualifier("clientSerializer") AbstractSerializer clientSerializer,
                                                            @Qualifier("petSerializer") AbstractSerializer petSerializer) {
        Map<Class, AbstractSerializer> serializers = new HashMap<>();
        serializers.put(Client.class, clientSerializer);
        serializers.put(Pet.class, petSerializer);
        return serializers;
    }

}
