package com.puc.doggis.register.configuration.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Dado buscado não encontrado")
public class ResourceNotFoundException  extends RuntimeException {

}
