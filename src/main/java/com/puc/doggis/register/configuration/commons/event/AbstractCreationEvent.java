package com.puc.doggis.register.configuration.commons.event;

import org.springframework.context.ApplicationEvent;

public abstract class AbstractCreationEvent<T> extends ApplicationEvent
{

    public AbstractCreationEvent(T source)
    {
        super(source);
    }
}
