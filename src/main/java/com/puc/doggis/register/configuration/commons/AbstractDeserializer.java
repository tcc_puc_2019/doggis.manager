package com.puc.doggis.register.configuration.commons;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public abstract class AbstractDeserializer<T> extends StdDeserializer<T>
{

    public AbstractDeserializer()
    {
        this(null);
    }

    public AbstractDeserializer(Class<?> vc)
    {
        super(vc);
    }

    public abstract T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException;

    public T deserialize(JsonNode obj, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {
        throw new UnsupportedOperationException("Desserialização não implementada");
    }

}
