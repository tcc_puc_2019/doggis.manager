package com.puc.doggis.register.acl.wrapper.commons;

import com.puc.doggis.register.acl.serializer.commons.PageSerializer;
import org.springframework.data.domain.Page;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Getter;

@JsonSerialize(using = PageSerializer.class)
@AllArgsConstructor
@Getter
public class PageWrapper
{
    private final Integer page;
    private final Integer itensPerPage;
    private final Class<?> entity;
    private final Page<Object> data;


    public long getTotalRegisters()
    {
        return data.getTotalElements();
    }

}
