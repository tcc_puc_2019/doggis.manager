package com.puc.doggis.register.acl.serializer.pet;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.puc.doggis.register.acl.wrapper.client.ClientWrapper;
import com.puc.doggis.register.acl.wrapper.pet.PetWrapper;
import com.puc.doggis.register.configuration.commons.AbstractSerializer;
import com.puc.doggis.register.configuration.label.client.ClientLabel;
import com.puc.doggis.register.configuration.label.pet.PetLabel;
import com.puc.doggis.register.domain.client.Client;
import com.puc.doggis.register.domain.pet.Pet;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@AllArgsConstructor
public class PetSerializer extends AbstractSerializer<PetWrapper>
{

    @Override
    public void serialize(PetWrapper userWrapper, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
    {
        this.serializeObject(userWrapper.getPet(),jsonGenerator);
    }

    @Override
    public void serializeObject(Object object, JsonGenerator jsonGenerator) throws IOException
    {
        this.serializeObject((Pet) object, jsonGenerator);
    }

    public void serializeObject(Pet pet, JsonGenerator jsonGenerator) throws IOException
    {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField(PetLabel.ID.getValue(), pet.getId());
        jsonGenerator.writeStringField(PetLabel.NAME.getValue(), pet.getName());
        jsonGenerator.writeStringField(PetLabel.TYPE.getValue(), pet.getType());
        jsonGenerator.writeStringField(PetLabel.RACE.getValue(), pet.getRace());
        jsonGenerator.writeStringField(PetLabel.PET_SIZE_TYPE.getValue(), pet.getPetSizeType().name());
        jsonGenerator.writeStringField(PetLabel.OBSERVATIONS.getValue(), pet.getObservations());
        jsonGenerator.writeBooleanField(PetLabel.CAN_TAKE_PICTURES.getValue(), pet.getCanTakePictures());

        jsonGenerator.writeObjectFieldStart(PetLabel.OWNER.getValue());
        jsonGenerator.writeNumberField(ClientLabel.ID.getValue(), pet.getOwner().getId());
        jsonGenerator.writeStringField(ClientLabel.NAME.getValue(), pet.getOwner().getName());
        jsonGenerator.writeEndObject();

        jsonGenerator.writeEndObject();
    }
}
