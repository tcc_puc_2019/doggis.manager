package com.puc.doggis.register.acl.wrapper.client;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.puc.doggis.register.acl.desserializer.user.UserDeserializer;
import com.puc.doggis.register.acl.serializer.client.ClientSerializer;
import com.puc.doggis.register.domain.client.Client;

import lombok.AllArgsConstructor;
import lombok.Getter;

@JsonDeserialize (using = UserDeserializer.class)
@JsonSerialize(using = ClientSerializer.class)
@AllArgsConstructor
@Getter
public class ClientWrapper
{
    private final Client user;

}
