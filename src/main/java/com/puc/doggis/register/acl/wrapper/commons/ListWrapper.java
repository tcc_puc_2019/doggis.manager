package com.puc.doggis.register.acl.wrapper.commons;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.puc.doggis.register.acl.serializer.commons.ListSerializer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@JsonSerialize(using = ListSerializer.class)
@AllArgsConstructor
@Getter
public class ListWrapper<T>
{
    private final Class<?> entity;
    private final List<Object> data;

}
