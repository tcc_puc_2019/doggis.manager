package com.puc.doggis.register.acl.desserializer.pet;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.puc.doggis.register.acl.wrapper.pet.PetWrapper;
import com.puc.doggis.register.configuration.commons.AbstractDeserializer;
import com.puc.doggis.register.configuration.enumerated.PetSizeType;
import com.puc.doggis.register.configuration.exception.ResourceNotFoundException;
import com.puc.doggis.register.configuration.label.client.ClientLabel;
import com.puc.doggis.register.configuration.label.pet.PetLabel;
import com.puc.doggis.register.domain.client.Client;
import com.puc.doggis.register.domain.pet.Pet;
import com.puc.doggis.register.infraestructure.repository.client.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static java.util.Objects.isNull;

@Service
@AllArgsConstructor
public class PetDeserializer extends AbstractDeserializer<PetWrapper>
{
    private final ClientRepository clientRepository;

    @Override
    public PetWrapper deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {
        JsonNode node = jsonParser.readValueAsTree();
        Pet pet = deserializeObject(node, deserializationContext);

        return new PetWrapper(pet);
    }

    public Pet deserializeObject(JsonNode node, DeserializationContext deserializationContext) throws IOException, JsonProcessingException
    {
        Long id = isNull(node.get(PetLabel.ID.getValue()))? null : node.get(PetLabel.ID.getValue()).asLong();
        JsonNode nameNode = node.get(PetLabel.NAME.getValue());
        JsonNode typeNode = node.get(PetLabel.TYPE.getValue());
        JsonNode raceNode = node.get(PetLabel.RACE.getValue());
        JsonNode petSizeNode = node.get(PetLabel.PET_SIZE_TYPE.getValue());
        JsonNode observationNode = node.get(PetLabel.OBSERVATIONS.getValue());
        JsonNode canTakePicturesNode = node.get(PetLabel.CAN_TAKE_PICTURES.getValue());
        JsonNode ownerNode = node.get(PetLabel.OWNER.getValue());

        Client client = clientRepository.findById(ownerNode.get(ClientLabel.ID.getValue()).asLong()).orElseThrow(ResourceNotFoundException::new);


        return new Pet(id,nameNode.asText(), typeNode.asText(), raceNode.asText(), PetSizeType.valueOf(petSizeNode.asText().toUpperCase()),
                observationNode.asText(), canTakePicturesNode.asBoolean(),client);
    }

}
