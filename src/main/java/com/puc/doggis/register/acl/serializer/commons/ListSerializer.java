package com.puc.doggis.register.acl.serializer.commons;

import java.io.IOException;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.puc.doggis.register.acl.wrapper.commons.ListWrapper;
import com.puc.doggis.register.configuration.commons.AbstractSerializer;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ListSerializer extends AbstractSerializer<ListWrapper>
{

    private final Map<Class, AbstractSerializer> objectSerializers;

    @Override
    public void serialize(ListWrapper listWrapper, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException
    {
        AbstractSerializer serializer = objectSerializers.get(listWrapper.getEntity());
        jsonGenerator.writeStartArray();
        for (Object object: listWrapper.getData())
        {
            serializer.serializeObject(object,jsonGenerator);
        }
        jsonGenerator.writeEndArray();
    }


}
