package com.puc.doggis.register.domain.client;


import com.puc.doggis.register.configuration.enumerated.UserType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Table(name = "client")
public class Client
{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "user_type")
    private UserType userType;

    @Column(name = "name")
    @Setter
    private String name;

    @Column(name = "email")
    @Setter
    private String email;

    @Column(name = "rg")
    @Setter
    private String rg;

    @Column(name = "cpf")
    @Setter
    private String cpf;

    @Column(name = "address")
    @Setter
    private String address;

    @Column(name = "pataz_amount")
    @Setter
    private Integer patazAmount;

}
