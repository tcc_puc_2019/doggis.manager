#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY src /usr/app/src
COPY pom.xml /usr/app

RUN mvn clean package -DskipTests
CMD java -Djava.security.egd=file:/dev/./urandom -Xmx500m -jar /usr/app/target/doggis-register-1.0-SNAPSHOT.jar